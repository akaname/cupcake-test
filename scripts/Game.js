//
// Handles Game Logic
//

function Game() {
    // pixi application
    this.app = new PIXI.Application(800, 600);

    // game stage
    this.stage = new PIXI.Container();

    // game renderer
    this.renderer;

    // game renderer setup
    this.setupRenderer();

    // game start menu
    this.startMenu;

    // game mode rules, player, etc
    this.mode;

    // game end menu
    this.endMenu;

    // game assets loader
    this.assetLoader = new AssetLoader(this.start.bind(this));
}

// setting up game renderer
Game.prototype.setupRenderer = function () {
    this.renderer = PIXI.autoDetectRenderer(
        800,
        600, {
            view: document.getElementById("game-canvas"),
            transparent: true,
            antialias: true,
        }
    );

    document.body.appendChild(this.renderer.view);

    // setting game canvas to the middle of the page
    this.renderer.view.style.position = 'absolute';
    this.renderer.view.style.left = '50%';
    this.renderer.view.style.top = '50%';
    // -55% instead of -50%, rule of design. Staying a bit off to the left feels better to the eye
    this.renderer.view.style.transform = 'translate3d( -55%, -50%, 0 )';
};

// starts game
Game.prototype.start = function (resources) {
    Audio.library = new AudioLibrary();
    Audio.library.addAudiosFromResources(resources);

    this.startMenu = new StartMenu();
    this.startMenu.intiaize(this.app.screen, this.app.ticker, this.stage, this.setGameMode.bind(this));

    this.endMenu = new EndMenu();
    this.endMenu.intiaize(this.app.screen, this.app.ticker, this.stage, this.reset.bind(this));

    this.app.ticker.add(this.update.bind(this));
};

// set new game mode
Game.prototype.setGameMode = function (mode) {
    this.mode = mode;
    this.mode.initialize(this.endMenu.show.bind(this.endMenu));
    this.newGame();
};

// creates new game
Game.prototype.newGame = function () {
    this.mode.newMode(this.app.screen, this.stage, this.app.ticker);
    this.mode.start();
};

// updates game
Game.prototype.update = function () {
    this.renderer.render(this.stage);
};

// resets game
Game.prototype.reset = function (toMenu = false) {
    if (toMenu) { // back to menu
        this.mode.end();
        this.startMenu.show();
    } else { // restart current mode
        this.mode.restart();
    }
};
