//
// Handles player logic
//

function Player() {
    this.id;
    this.screen;
    this.container;
    this.background;
    this.handPicks;
    this.endSelectionCallback;
}

// player initializing
Player.prototype.intiaize = function (screen, stage, ticker, handsSubclasses, callback, index) {
    this.id = index;
    this.screen = screen;
    this.endSelectionCallback = callback;

    this.container = new PIXI.Container();
    stage.addChild(this.container);

    var info = this.getBackgroundInfo(index);

    this.background = new Background();
    this.background.initialize(this.container, ticker, info);
    this.background.visible(false);

    this.handPicks = new HandPicking(handsSubclasses);
    this.handPicks.initialize(this.background.getGraphics(), ticker, info, this.onHandSelection.bind(this));
};

// starts hand selection
Player.prototype.reset = function () {
    this.handPicks.reset(this.background.getGraphics());
};

// starts hand selection
Player.prototype.remove = function () {
    this.handPicks.remove(this.background.getGraphics());
    this.handPicks = null;
    this.background.remove();
    this.background = null;
};

// starts hand selection
Player.prototype.startHandSelection = function () {
    this.background.visible(true);
    this.background.toCenter(this.handSelection.bind(this));
    this.handPicks.setVisibility(true);
    this.handPicks.setInterection(true);
};

// show hand at the offset background position
Player.prototype.showHand = function () {
    this.handPicks.showSelectedHand(this.background.getOffsetPosition());
    this.background.toOrigin();
};

// starts outcome animation for lost or draw
Player.prototype.outcomeDrawAnimation = function () {
    this.background.toOutside(null, false, false);
};

// starts outcome animation for lost or draw
Player.prototype.outcomeLooseAnimation = function () {
    this.background.toOutsideByDirection();
};

// starts outcome animation for win
Player.prototype.outcomeWinAnimation = function (background) {
    this.handPicks.getSelectedHand().expand();
    this.background.toAnotherBackground(background);
};

//
// GETTERS
//

// return the selected hand
Player.prototype.getSelection = function () {
    return this.handPicks.getSelectedHand();
};

// gets player info based on index
Player.prototype.getBackgroundInfo = function (indexOfPlayer) {
    var playerInfo = new BackgroundInfo();

    playerInfo.points = [
            100, 0,
            0, this.screen.height,
            this.screen.width - 100, this.screen.height,
            this.screen.width, 0
    ];

    if (indexOfPlayer == 0) {
        playerInfo.color = 0x46A2FF;
        playerInfo.flip = 1;

        playerInfo.pivot.x = this.screen.width;
        playerInfo.pivot.y = this.screen.height * 0.5;

        playerInfo.origin.x = this.screen.width * 0.5 + 50;
        playerInfo.origin.y = this.screen.height * 0.5;

        playerInfo.center.x = this.screen.width;
        playerInfo.center.y = this.screen.height * 0.5;

        playerInfo.outside.x = 0;
        playerInfo.outside.y = this.screen.height * 0.5;

        playerInfo.selectionOffset.x = this.screen.width * 0.75;
        playerInfo.selectionOffset.y = this.screen.height * 0.5;

        return playerInfo;
    } else if (indexOfPlayer == 1) {
        playerInfo.color = 0xff4646;
        playerInfo.flip = -1;

        playerInfo.pivot.x = 0;
        playerInfo.pivot.y = this.screen.height * 0.5;

        playerInfo.origin.x = this.screen.width * 0.5 - 50;
        playerInfo.origin.y = this.screen.height * 0.5;

        playerInfo.center.x = 0;
        playerInfo.center.y = this.screen.height * 0.5;

        playerInfo.outside.x = this.screen.width;
        playerInfo.outside.y = this.screen.height * 0.5;

        playerInfo.selectionOffset.x = this.screen.width * 0.25;
        playerInfo.selectionOffset.y = this.screen.height * 0.5;

        return playerInfo;
    }

    console.error('Player index out of bounds');
};

//
// CALLBACKS
//

// after background toCenter movement function
Player.prototype.handSelection = function () {
    this.handPicks.setVisibility(true);
};

// when hand is selected
Player.prototype.onHandSelection = function (handIndex) {
    this.handPicks.setInterection(false);
    this.handPicks.selectedHand = handIndex;

    this.background.toOutside(this.endHandSelection.bind(this));
};

// after background toOutside movement function
Player.prototype.endHandSelection = function () {
    if (this.endSelectionCallback) {
        this.endSelectionCallback();
    }
};
