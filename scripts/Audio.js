//
// Handles game audio 
//

var Audio = {
    //audio library
    library: new AudioLibrary(), // default library

    // audio tags
    TAGS: {
        SELECT: {
            NAME: 'select',
            PATH: 'select.mp3'
        },
        ROCK: {
            NAME: 'rock',
            PATH: 'rock.mp3'
        },
        PAPER: {
            NAME: 'paper',
            PATH: 'paper.mp3'
        },
        SCISSORS: {
            NAME: 'scissors',
            PATH: 'scissors.mp3'
        },
        SPOCK: {
            NAME: 'spock',
            PATH: 'spock.mp3'
        },
        LIZARD: {
            NAME: 'lizard',
            PATH: 'lizard.mp3'
        },
    }
};

// handles game audio library
function AudioLibrary() {
    var audios = {};

    this.addAudiosFromResources = function (resources) {
        for (var tag in Audio.TAGS) {
            var audioName = Audio.TAGS[tag].NAME;
            var audio = resources[audioName];

            if (audio) {
                audios[audioName] = audio.sound;
            }
        }
    }

    this.getAudio = function (tag) {
        return audios[tag];
    }
}
