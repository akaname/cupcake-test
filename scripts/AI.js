//
// Handles AI logic
//

// setting AI inheritence
AI.prototype = Object.create(Player.prototype);
AI.prototype.constructor = AI;

// Player enemy
function AI() {
    Player.call(this);
}

// starts AI hand selection
AI.prototype.startHandSelection = function (outcome, ruling) {
    this.background.visible(true);
    this.background.toOutside(null, true);
    this.handPicks.setVisibility(true);

    //AI decision making based on the chinese article "Social cycling and conditioning response in the rock paper scissors game", basically
    // if player wins, they stay
    // if player loose, they shift

    // basic decision, get random pick
    var hand = Math.floor(Math.random() * this.handPicks.hands.length);

    // last round information
    if (outcome) {
        // not always follow the rule
        if (Math.random() > 0.25) {
            if (outcome.hasWinner()) {
                var options = [];

                // case lost, the winner opponent will most likely stay with same movement
                // so counter his hand
                if (outcome.getPlayerLooser().id === this.id) {
                    var counters = ruling.getCounters(outcome.getWinnerMove().winner);

                    options = this.handPicks.hands.filter(h => counters.find(function (c) {
                        return h.constructor === c.winner;
                    }));
                }
                //case win, the looser opponent will most likely shift his movement
                // countering the winner, so counter his counter movement
                else {
                    var aiCounters = ruling.getCounters(outcome.getWinnerMove().winner);

                    var aiCountersFilter = aiCounters.filter(c => this.handPicks.hands.find(function (h) {
                        return c.winner === h.constructor;
                    }));

                    var otherCounters = [];

                    for (var counter in aiCountersFilter) {
                        otherCounters = otherCounters.concat(ruling.getCounters(aiCountersFilter[counter].winner));
                    }

                    options = this.handPicks.hands.filter(h => otherCounters.find(function (c) {
                        return h.constructor === c.winner;
                    }));
                }

                if (options.length > 0) {
                    var choice = Math.floor(Math.random() * options.length);
                    hand = options[choice].index;
                }
            }
        }
    }

    this.handPicks.selectedHand = hand;
    this.endHandSelection();
};
