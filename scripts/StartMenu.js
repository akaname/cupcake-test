//
// Handles game mode selection
//

function StartMenu() {
    this.continueCallback;
    this.area;
    this.title;
    this.buttons;
};

// initializes start menu
StartMenu.prototype.intiaize = function (screen, ticker, stage, continueCallback) {
    this.continueCallback = continueCallback;
    this.area = new PIXI.Container();
    this.title = new StartMenuTitle();
    this.background = new Background();

    //start menu buttons
    var classicGameButton = new MenuButton('Classic Game', this.onSelectCallback.bind(this, new GameMode()));
    var expandedGameButton = new MenuButton('Expanded Game', this.onSelectCallback.bind(this, new ExpandedMode()));
    this.buttons = [classicGameButton, expandedGameButton];

    this.addToCanvas(stage, screen);
    this.background.initialize(stage, ticker, this.getAreaInfo(screen));
    this.background.getGraphics().addChild(this.area);
};

// gets area info for background
StartMenu.prototype.getAreaInfo = function (screen) {
    var info = new BackgroundInfo();

    info.outside.x = 0;
    info.outside.y = -screen.height;

    return info;
};

// adds start menu contents to area container
StartMenu.prototype.addToCanvas = function (stage, screen) {
    this.area.addChild(this.title.font);

    for (var button in this.buttons) {
        this.area.addChild(this.buttons[button].getContainer());
        this.buttons[button].setOffset(0,
            (screen.height * 0.1) + (screen.height * 0.125) * button);
    }

    this.area.position.set(screen.width * 0.5, screen.height * 0.5);
};

// start menu button selection callback
StartMenu.prototype.onSelectCallback = function (mode) {
    this.hide(this.continueCallback.bind(this, mode));
};

// hide start menu
StartMenu.prototype.hide = function (callback = null, teleport = false) {
    for (var button in this.buttons) {
        this.buttons[button].setInteractivity(false);
    }

    this.background.toOutside(callback, teleport);
};

// shows start menu
StartMenu.prototype.show = function (callback = null, teleport = false) {
    for (var button in this.buttons) {
        this.buttons[button].setInteractivity(true);
    }

    this.background.toCenter(callback, teleport);
};

// handles start menu title
function StartMenuTitle() {
    var text = 'Cupcake\nDev\nTest';
    this.font = new PIXI.extras.BitmapText(text, {
        font: '40 ' + Fonts.TAGS.MAIN.NAME,
        align: 'center',
    });
    this.font.anchor.set(0.5, 0.5);
    this.font.position.y = -150;
}

// handles start menu buttons
function MenuButton(text, callback) {
    var background;
    var font;
    var sound;

    // setting button text
    font = new PIXI.extras.BitmapText(text, {
        font: '24 ' + Fonts.TAGS.MAIN.NAME,
        align: 'center',
    });
    font.anchor.set(0.5, 0.5);

    // setting button pointerdown sound
    sound = Audio.library.getAudio(Audio.TAGS.SELECT.NAME);

    // setting button background
    background = new PIXI.Graphics();
    background.lineStyle(5, 0x0a0a0a, 1);
    background.beginFill(0x151515, 1);

    var width = font.textWidth + 40;
    var height = font.textHeight + 20;

    background.drawRoundedRect(-width * 0.5, -height * 0.5, width, height, 20);
    background.addChild(font);

    background.on('pointerdown', () => {
        if (callback) {
            if (sound) sound.play();
            callback();
        }
    });

    // pointer over trigger: mouse only
    background.on('pointerover', () => {
        background.alpha = 0.75;
    });

    // pointer out trigger: mouse only
    background.on('pointerout', () => {
        background.alpha = 1;
    });

    // setters

    this.getContainer = function () {
        return background;
    }

    // setters

    this.setOffset = function (x, y) {
        background.position.set(x, y);
    }

    this.setInteractivity(true);
}

// sets button interection
MenuButton.prototype.setInteractivity = function (interection) {
    // sets button to clickable or not
    this.getContainer().interactive = interection;

    // show/hide hand cursor
    this.getContainer().buttonMode = interection;

    // showing button background 
    this.getContainer().alpha = 1;
};
