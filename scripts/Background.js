//
// Handles PIXI Graphics showing
//

function Background() {
    var graphics;
    var ticker;
    var info;
    var moveToPosition;
    var moveToCallback;

    //getters
    this.getGraphics = function () {
        return graphics;
    }

    this.getTicker = function () {
        return ticker;
    }

    this.getInfo = function () {
        return info;
    }

    //setters
    this.setGraphics = function (value) {
        graphics = value;
    }

    this.setTicker = function (value) {
        ticker = value;
    }

    this.setInfo = function (value) {
        info = value;
    }

    this.moveToPosition = function (value) {
        if (value) {
            moveToPosition = value;
        } else {
            return moveToPosition;
        }
    }

    this.moveToCallback = function (value) {
        if (value) {
            moveToCallback = value;
        } else {
            return moveToCallback;
        }
    }

    // move to position interval
    this.interval;
}

// initializes background
Background.prototype.initialize = function (container, ticker, info) {
    this.setTicker(ticker);
    this.setInfo(info);
    this.setGraphics(new PIXI.Graphics());
    this.getGraphics().beginFill(info.color, 1);
    this.getGraphics().drawPolygon(info.points);
    this.getGraphics().pivot.set(info.pivot.x, info.pivot.y);
    this.getGraphics().origin = info.origin;
    this.getGraphics().position.set(info.center.x, info.center.y);

    container.addChild(this.getGraphics());
};

// background start move to origin
Background.prototype.toOrigin = function (callback, teleport = false) {
    this.getTicker().remove(this.moveTo, this);

    this.getGraphics().position.set(this.getInfo().outside.x, this.getInfo().outside.y);

    this.moveToPosition(this.getInfo().origin);
    this.interval = 0;

    this.moveToCallback = callback;

    if (teleport) {
        if (callback) {
            callback();
        }
    } else {
        this.getTicker().add(this.moveTo, this);
    }
};

// background start move to center
Background.prototype.toCenter = function (callback, teleport = false, setPosition = true) {
    this.getTicker().remove(this.moveTo, this);

    this.moveToPosition(this.getInfo().center);

    if (teleport) {
        this.getGraphics().position.set(this.moveToPosition().x, this.moveToPosition().y);
        if (callback) {
            callback();
        }
    } else {
        if (setPosition) {
            this.getGraphics().position.set(this.getInfo().outside.x, this.getInfo().outside.y);
        }

        this.interval = 0;

        this.moveToCallback = callback;

        this.getTicker().add(this.moveTo, this);
    }
};

// background start move to outside
Background.prototype.toOutside = function (callback, teleport = false, setPosition = true) {
    this.getTicker().remove(this.moveTo, this);

    this.moveToPosition(this.getInfo().outside);

    if (teleport) {
        this.getGraphics().position.set(this.moveToPosition().x, this.moveToPosition().y);
        if (callback) {
            callback();
        }
    } else {
        if (setPosition) {
            this.getGraphics().position.set(this.getInfo().center.x, this.getInfo().center.y);
        }

        this.interval = 0;

        this.moveToCallback = callback;

        this.getTicker().add(this.moveTo, this);
    }
};

// background start move to outside by direction
Background.prototype.toOutsideByDirection = function () {
    this.getTicker().remove(this.moveTo, this);

    var dir = Math.vector2.direction(this.getGraphics().position, this.getInfo().outside).normalized;

    dir.x *= this.getGraphics().width * 1.5;
    dir.y *= this.getGraphics().height;

    var position = {
        x: this.getGraphics().position.x + dir.x,
        y: this.getGraphics().position.y + dir.y,
    };

    this.moveToPosition(position);
    this.interval = 0;

    this.moveToCallback = null;

    this.getTicker().add(this.moveTo, this);
};

// moves background based on another's background position
Background.prototype.toAnotherBackground = function (background) {
    this.getTicker().remove(this.moveTo, this);

    var dir = Math.vector2.direction(this.getGraphics().position, background.getGraphics().position).normalized;

    dir.x *= this.getGraphics().width * 1.5;
    dir.y *= this.getGraphics().height;

    var position = {
        x: this.getGraphics().position.x - dir.x,
        y: this.getGraphics().position.y - dir.y,
    };

    this.moveToPosition(position);
    this.interval = 0;
    this.moveToCallback = null;
    this.getTicker().add(this.moveTo, this);
};

// background move function
Background.prototype.moveTo = function (deltaTime) {
    this.interval += deltaTime * 0.005;

    if (Math.vector2.distance(this.getGraphics().position, this.moveToPosition()) < 1) {
        this.getGraphics().position.set(this.moveToPosition().x, this.moveToPosition().y);

        this.getTicker().remove(this.moveTo, this);

        if (this.moveToCallback) {
            this.moveToCallback();
        }

        return;
    }

    var x = Math.lerp(this.getGraphics().position.x, this.moveToPosition().x, this.interval);
    var y = Math.lerp(this.getGraphics().position.y, this.moveToPosition().y, this.interval);

    this.getGraphics().position.set(x, y);
};

// return background info selection position
Background.prototype.getOffsetPosition = function () {
    return this.getInfo().selectionOffset;
};

// background visibility
Background.prototype.visible = function (visibility) {
    this.getGraphics().visible = visibility;
}

// removes background
Background.prototype.remove = function () {
    var parent = this.getGraphics().parent;
    parent.removeChild(this.getGraphics());
};

//
// Handles background info
//

function BackgroundInfo() {
    this.color = 0x000000;
    this.flip = 1;
    this.points = [];
    this.pivot = {
        x: 0,
        y: 0
    };
    this.origin = {
        x: 0,
        y: 0
    };
    this.center = {
        x: 0,
        y: 0
    };
    this.outside = {
        x: 0,
        y: 0
    };
    this.selectionOffset = {
        x: 0,
        y: 0
    };
}
