//
// Standard rock, paper, scissors game
//

function GameMode() {
    // mode rules
    this.ruling;
    // mode player
    this.players;
    // current active player
    this.currentPlayer;
    // game outcome
    this.outcome;
    // game outcome display
    this.outcomeDisplay;
    // callback to after game mode outcome result
    this.endMenuCallback;
}

// initializes game mode
GameMode.prototype.initialize = function (endMenuCallback) {
    this.endMenuCallback = endMenuCallback;
    this.ruling = new Ruling();
    this.currentPlayer = 0;
};

// starts new mode
GameMode.prototype.newMode = function (screen, stage, ticker) {
    var handSubclasses = this.subClasses();

    var player1 = new Player();
    player1.intiaize(screen, stage, ticker, handSubclasses, this.onSelectHand.bind(this), 0);

    var player2 = new AI();
    player2.intiaize(screen, stage, ticker, handSubclasses, this.onSelectHand.bind(this), 1);

    this.players = [player1, player2];

    this.outcomeDisplay = new OutcomeDisplay();
    this.outcomeDisplay.initialize(screen, stage, ticker);
};

// returns game mode hands subclasses
GameMode.prototype.subClasses = function () {
    return HandMove.getClassicSubclasses();
};


// start game
GameMode.prototype.start = function () {
    this.selectHand();
};

// restarts current mode
GameMode.prototype.restart = function () {
    this.currentPlayer = 0;

    for (var player in this.players) {
        this.players[player].reset();
    }

    this.outcomeDisplay.reset();

    this.start();
};

// restarts current mode
GameMode.prototype.end = function () {
    for (var player in this.players) {
        this.players[player].remove();
    }

    this.outcomeDisplay.remove();

    this.players.length = 0;
};

// starts player hand selection
GameMode.prototype.selectHand = function () {
    if (this.currentPlayer < this.players.length) {
        this.players[this.currentPlayer].startHandSelection(this.outcome, this.ruling);
    } else {
        this.pullOutcome();
    }
};

// callback to when player confirm selection of hand
GameMode.prototype.onSelectHand = function () {
    this.currentPlayer++;
    this.selectHand();
};

// show game outcome
GameMode.prototype.pullOutcome = function () {
    for (var i = 0; i < this.players.length; i++) {
        this.players[i].showHand();
    }

    this.outcome = new Outcome(
        this.players,
        this.ruling.getWinner(this.players[0], this.players[1])
    );

    this.outcomeDisplay.expose(this.outcome, this.animateOutcome.bind(this))
};

// handles outcome animation
GameMode.prototype.animateOutcome = function () {
    // play draw animation
    if (!this.outcome.hasWinner()) {
        for (var player in this.players) {
            this.players[player].outcomeDrawAnimation();
        }
    } else {
        this.outcome.getPlayerWinner().outcomeWinAnimation(this.outcome.getPlayerLooser().background);
        this.outcome.getPlayerLooser().outcomeLooseAnimation();
    }

    setTimeout(this.endMenuCallback, Delays.RESET);
};

/*
 *
 *   OTHER MODES
 *
 */

// setting ExpandedMode inheritence
ExpandedMode.prototype = Object.create(GameMode.prototype);
ExpandedMode.prototype.constructor = ExpandedMode;

// rock, paper, scissors, lizzard spock game
function ExpandedMode() {
    GameMode.call(this);
}

// returns game mode hands subclasses
ExpandedMode.prototype.subClasses = function () {
    return HandMove.getExpandedSubclasses();
};
