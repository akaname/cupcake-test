//
//  Game outcome
//

function Outcome(players, winner) {
    var hasWinner = (winner) ? true : false;
    var playerWinner;
    var playerLooser;

    if (hasWinner) {
        for (var player in players) {
            if (players[player].getSelection() instanceof winner.winner) {
                playerWinner = players[player];
            } else {
                playerLooser = players[player];
            }
        }
    }

    var message = (hasWinner) ?
        '{0} {1} {2}!'.format(winner.winner.name, winner.phrase, winner.loser.name) :
        message = 'DRAW!';


    this.hasWinner = function () {
        return hasWinner;
    }

    // getters

    this.getWinnerMove = function () {
        return winner;
    }

    this.getMessage = function () {
        return message;
    }

    this.getSpacedMessage = function () {
        var spacedMessage = message.replace(/ /g, '\n');
        return spacedMessage;
    }

    this.getPlayerWinner = function () {
        return playerWinner;
    }

    this.getPlayerLooser = function () {
        return playerLooser;
    }
}

//
// Handles game outcome display
//

function OutcomeDisplay() {
    this.outcomeText;
    this.stopCallback;
    this.stage;
    this.ticker
}

// intializes game outcome
OutcomeDisplay.prototype.initialize = function (screen, stage, ticker) {
    this.stage = stage;
    this.ticker = ticker;

    this.outcomeText = new PIXI.extras.BitmapText('', {
        font: '40 ' + Fonts.TAGS.MAIN.NAME,
        align: 'center',
    });

    this.outcomeText.anchor.set(0.5, 0.5);
    this.outcomeText.position.x = screen.width * 0.5;

    var textOffsetY = screen.height * 0.25;

    this.outcomeText.position.y = textOffsetY;
    this.outcomeText.visible = false;
};

// sets outcomeText
OutcomeDisplay.prototype.expose = function (outcome, callback) {
    this.stage.addChild(this.outcomeText);

    this.outcomeText.text = outcome.getMessage();

    if (this.outcomeText.textWidth > screen.width) {
        this.outcomeText.text = outcome.getSpacedMessage();
    }

    setTimeout(callback, Delays.WAIT_FOR_BLINK);
    setTimeout(this.showOutcome.bind(this), Delays.SHOW_OUTCOME);
};

// shows outcomeText and start blink animation
OutcomeDisplay.prototype.showOutcome = function () {
    this.outcomeText.visible = true;
    this.outcomeText.visibleTimer = 0;

    this.ticker.add(this.blink, this);
};

// outcomeText blink animation
OutcomeDisplay.prototype.blink = function (delta) {
    this.outcomeText.visibleTimer += delta * 0.025;

    if (this.outcomeText.visible) {
        if (this.stopCallback) {
            this.outcomeText.visibleTimer += delta * 0.025;
        }

        if (this.outcomeText.visibleTimer > 1.25) {
            this.outcomeText.visibleTimer = 0;
            this.outcomeText.visible = false;
        }
    } else {
        if (this.stopCallback) {
            this.stopCallback();
            return;
        } else if (this.outcomeText.visibleTimer > 0.75) {
            this.outcomeText.visibleTimer = 0;
            this.outcomeText.visible = true;
        }
    }
};

// stops blink animation
OutcomeDisplay.prototype.stopBlink = function () {
    this.ticker.remove(this.blink, this);
    this.stage.removeChild(this.outcomeText);
    this.stopCallback = null;
};

// resets outcome display
OutcomeDisplay.prototype.reset = function () {
    this.stopCallback = this.stopBlink.bind(this);
};

// destroys outcome display
OutcomeDisplay.prototype.remove = function () {
    this.outcomeText.destroy();
};
