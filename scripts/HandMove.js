// setting HandMove inheritence
HandMove.prototype = Object.create(PIXI.Sprite.prototype);
HandMove.prototype.constructor = HandMove;

// hand moves tags
HandMove.TAGS = {
	// path to the sprite sheet json
	JSON_PATH: 'hand_moves.json',

	// sprite sheet name
	JSON_NAME: 'hand_moves',

	// path to the halo sprite
	HALO_PATH: 'halo.png',

	// halo sprite name
	HALO_NAME: 'halo',
};

//
// Base class of all hands (e.g. rock, paper, scissors, etc)
//

function HandMove(spriteID) {
	var texture = new PIXI.Texture.fromFrame(spriteID);
	PIXI.Sprite.call(this, texture);

	// hand halo
	this.halo;

	// game ticker
	this.ticker;

	// hand index
	this.index;

	// hand base size
	this.baseSize;

	// hand focus size
	this.focusSize;

	// sound when hand selected
	this.selectSound;

	// sound when hand wins
	this.winSound;
}

// initializes halo
HandMove.prototype.initializeHalo = function (container) {
	this.halo = new PIXI.Sprite.fromImage(HandMove.TAGS.HALO_NAME)
	this.halo.anchor.set(0.5);
	this.halo.alpha = 0.5;

	container.addChild(this.halo);
};

// initializes hand
HandMove.prototype.initializeHand = function (container, ticker, flip, index) {
	// setting ticker
	this.ticker = ticker;

	// setting hand index
	this.index = index;

	// setting anchor to the middle of the sprite
	this.anchor.set(0.5);

	// setting hand flip
	this.scale.x *= flip;

	// adding hand to canvas
	container.addChild(this);

	//setting select sounds
	this.selectSound = Audio.library.getAudio(Audio.TAGS.SELECT.NAME);
};

// initializes interection
HandMove.prototype.initializeInterection = function (callback) {
	// pointer down: touch and mouse
	this.on('pointerdown', () => {
		if (this.selectSound) {
			this.selectSound.play();
		}
		callback(this.index);
	});

	// pointer over trigger: mouse only
	this.on('pointerover', () => {
		this.halo.visible = true;
	});

	// pointer out trigger: mouse only
	this.on('pointerout', () => {
		this.halo.visible = false;
	});
};

// focus on hand
HandMove.prototype.focus = function (position) {
	this.visible = true;
	this.position.set(position.x, position.y);

	this.width = this.focusSize;
	this.height = this.focusSize;
};

// expands hand
HandMove.prototype.expand = function () {
	if (this.winSound) {
		this.winSound.play();
	}
	this.width = this.width * 1.5;
	this.height = this.height * 1.5;
};

// shrink hand
HandMove.prototype.shrink = function () {
	this.width = this.baseSize;
	this.height = this.baseSize;
};

//
// getters
//

// gets hands classic subclasses
HandMove.getClassicSubclasses = function () {
	var allVars = Object.keys(window);

	var hands = allVars.filter(function (key) {
		try {
			// will trigger warning due to Chrome getter on window.webkitStorageInfo
			var obj = window[key];

			if (obj.prototype instanceof HandMove) {
				return (obj === Rock ||
					obj === Paper ||
					obj === Scissors);
			}
			2
		} catch (e) {
			return false;
		}
	});
	return hands;
};

// gets hands expanded subclasses
HandMove.getExpandedSubclasses = function () {
	var allVars = Object.keys(window);

	var hands = allVars.filter(function (key) {
		try {
			// will trigger warning due to Chrome getter on window.webkitStorageInfo
			var obj = window[key];
			return obj.prototype instanceof HandMove;
		} catch (e) {
			return false;
		}
	});
	return hands;
};

//
// setters
//

// sets hand interection
HandMove.prototype.setInterection = function (interection) {

	// sets sprite to clickable or not
	this.interactive = interection;

	// show/hide hand cursor
	this.buttonMode = interection;

	// hide halo
	if (!interection) {
		this.halo.visible = interection;
	}
};

// sets position of both hand and halo
HandMove.prototype.setPosition = function (x, y) {
	this.halo.position.set(x, y);
	this.position.set(x, y);
};

// resizes both hand and halo
HandMove.prototype.setSize = function (size, focus) {
	this.halo.width = size * 1.25;
	this.halo.height = size * 1.25;

	this.width = size * 0.95;
	this.height = size * 0.95;

	// setting base scale after parenting
	this.baseSize = this.width;

	// setting focus scale
	this.focusSize = focus;
};


/*
 *
 *  Hand Types
 *
 */

//
// Rock
//

// setting Rock inheritence
Rock.prototype = Object.create(HandMove.prototype);
Rock.prototype.constructor = Rock;

// Rock hand type
function Rock() {
	HandMove.call(this, 'rock');
	this.winSound = Audio.library.getAudio(Audio.TAGS.ROCK.NAME);
}

//
// Paper
//

// setting Paper inheritence
Paper.prototype = Object.create(HandMove.prototype);
Paper.prototype.constructor = Paper;

// Paper hand type
function Paper() {
	HandMove.call(this, 'paper');
	this.winSound = Audio.library.getAudio(Audio.TAGS.PAPER.NAME);

}

//
// Scissors
//

// setting Scissors inheritence
Scissors.prototype = Object.create(HandMove.prototype);
Scissors.prototype.constructor = Scissors;

// Scissors hand type
function Scissors() {
	HandMove.call(this, 'scissors');
	this.winSound = Audio.library.getAudio(Audio.TAGS.SCISSORS.NAME);
}

//
// Spock
//

// setting Spock inheritence
Spock.prototype = Object.create(HandMove.prototype);
Spock.prototype.constructor = Spock;

// Spock hand type
function Spock() {
	HandMove.call(this, 'spock');
	this.winSound = Audio.library.getAudio(Audio.TAGS.SPOCK.NAME);
}

//
// Lizard
//

// setting Lizard inheritence
Lizard.prototype = Object.create(HandMove.prototype);
Lizard.prototype.constructor = Lizard;


// Lizard hand type
function Lizard() {
	HandMove.call(this, 'lizard');
	this.winSound = Audio.library.getAudio(Audio.TAGS.LIZARD.NAME);

}
