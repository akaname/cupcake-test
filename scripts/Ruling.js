//
// handles game rules
//

function Ruling() {
	this.norms = [];
	this.norms.push(new Norm(Rock, 'crushes', Scissors));
	this.norms.push(new Norm(Rock, 'crushes', Lizard));
	this.norms.push(new Norm(Paper, 'wraps', Rock));
	this.norms.push(new Norm(Paper, 'disproves', Spock));
	this.norms.push(new Norm(Scissors, 'cuts', Paper));
	this.norms.push(new Norm(Scissors, 'decapitates', Lizard));
	this.norms.push(new Norm(Spock, 'smashes', Scissors));
	this.norms.push(new Norm(Spock, 'vaporizes', Rock));
	this.norms.push(new Norm(Lizard, 'poisons', Spock));
	this.norms.push(new Norm(Lizard, 'eats', Paper));

	var rock = new Rock();
	var paper = new Paper();
	var scissors = new Scissors();
}

// return winner based in rules
Ruling.prototype.getWinner = function (player1, player2) {
	var winner;

	winner = this.norms.find(n => player1.getSelection() instanceof n.winner && player2.getSelection() instanceof n.loser);

	if (winner) return winner;

	winner = this.norms.find(n => player2.getSelection() instanceof n.winner && player1.getSelection() instanceof n.loser);

	if (winner) return winner;

	return null;
};

// gets counter to specific move
Ruling.prototype.getCounters = function (move) {
	return this.norms.filter(n => n.loser === move);
}

// rule norm
function Norm(winner, phrase, loser) {
	this.winner = winner;
	this.phrase = phrase;
	this.loser = loser;
}
