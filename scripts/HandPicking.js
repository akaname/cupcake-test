//
// Handles the hand picking: creating hands
//

function HandPicking(subclasses) {
	this.ticker;
	this.hands = [];
	this.selectedHand;
	this.createHands(subclasses);
}

// creates Rock, Paper, Scissors
HandPicking.prototype.createHands = function (subclasses) {
	// creates all subclasses
	var hands = subclasses.map(function (c) {
		return new window[c]
	});

	// adding created subclasses
	this.hands = this.hands.concat(hands);
};

// initializes each hand
HandPicking.prototype.initialize = function (container, ticker, info, callback) {

	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].initializeHalo(container);
	}

	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].initializeHand(container, ticker, info.flip, i);
		this.hands[i].setInterection(false);
		this.hands[i].initializeInterection(callback);

	}

	this.reset(container);
};

// resets each hand
HandPicking.prototype.reset = function (container) {
	this.selectedHand = 0;
	this.setVisibility(false);
	this.setPosition(container);

	for (var hand in this.hands) {
		this.hands[hand].shrink();
	}
};

// removes each hand
HandPicking.prototype.remove = function () {
	for (var hand in this.hands) {
		this.hands[hand].destroy();
	}

	this.hands.length = 0;
};

// shows selected hand
HandPicking.prototype.showSelectedHand = function (position) {
	this.setVisibility(false, this.selectedHand);
	this.getSelectedHand().focus(position);
};


//
// GETTERS
//

HandPicking.prototype.getSelectedHand = function () {
	return this.hands[this.selectedHand];
};


//
// SETTER
//

// sets each hand visibility
HandPicking.prototype.setVisibility = function (visibility, exception = null) {
	for (var i = 0; i < this.hands.length; i++) {
		if (exception) {
			if (i === exception) {
				continue;
			}
		}

		this.hands[i].visible = visibility;
	}
};

// sets each hand position
HandPicking.prototype.setPosition = function (container) {
	var min = container.width * 0.22;
	var max = (container.width * 0.78);
	var size = container.width * 0.75 / this.hands.length;
	var focusSize = container.width * 0.25;
	var amount = 0;

	for (var i = 0; i < this.hands.length; i++) {
		amount = i / (this.hands.length - 1);

		this.hands[i].setSize(size, focusSize);
		this.hands[i].setPosition(
			Math.lerp(min, max, amount),
			container.height / 2
		);
	}
};

// sets hand interection
HandPicking.prototype.setInterection = function (interection) {
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].setInterection(interection);
	}
};
