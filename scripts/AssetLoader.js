//
// Handles asset loading: sprites, fonts, sounds, etc
//

function AssetLoader(onLoadFinishedCallback) {
    this.callback = onLoadFinishedCallback;
    this.load();
}

// called to start loading assets
AssetLoader.prototype.load = function () {
    // assets paths
    var assets = 'assets/';
    var textures = 'textures/';
    var fonts = 'fonts/';
    var audios = 'audios/';

    // PIXI loader
    var loader = PIXI.loader;

    // adding textures
    loader.add(HandMove.TAGS.JSON_NAME, assets + textures + HandMove.TAGS.JSON_PATH);
    loader.add(HandMove.TAGS.HALO_NAME, assets + textures + HandMove.TAGS.HALO_PATH);

    // adding fonts
    loader.add(Fonts.TAGS.MAIN.NAME, assets + fonts + Fonts.TAGS.MAIN.PATH);

    // adding audios
    loader.add(Audio.TAGS.SELECT.NAME, assets + audios + Audio.TAGS.SELECT.PATH);
    loader.add(Audio.TAGS.ROCK.NAME, assets + audios + Audio.TAGS.ROCK.PATH);
    loader.add(Audio.TAGS.PAPER.NAME, assets + audios + Audio.TAGS.PAPER.PATH);
    loader.add(Audio.TAGS.SCISSORS.NAME, assets + audios + Audio.TAGS.SCISSORS.PATH);
    loader.add(Audio.TAGS.SPOCK.NAME, assets + audios + Audio.TAGS.SPOCK.PATH);
    loader.add(Audio.TAGS.LIZARD.NAME, assets + audios + Audio.TAGS.LIZARD.PATH);

    // progress callback
    loader.on('progress', this.onProgress, this);

    // error callback
    loader.on('error', this.onError, this);

    // comlete callback
    loader.once('complete', this.onFinished.bind(this));

    // start load
    loader.load();
};

// called once per loaded/errored file
AssetLoader.prototype.onProgress = function (loader, resources) {
    console.log('{0} loaded. Progress: {1}%'.format(resources.name, resources.progressChunk));
};

// called once per errored file
AssetLoader.prototype.onError = function (err, loader, resource) {
    console.log('{0} at {1}%'.format(err, resource.name));
};

// called once when the queued resources all load
AssetLoader.prototype.onFinished = function (loader, resources) {
    console.log('Complete loading resources');

    this.callback(resources);
};
