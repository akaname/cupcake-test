//
// Handles Help classes or functions
//

// <Helper> Extending Math to add lerp function
Math.lerp = function (value1, value2, amount) {
	amount = amount < 0 ? 0 : amount;
	amount = amount > 1 ? 1 : amount;
	return value1 + (value2 - value1) * amount;
};

// <Helper> Extending Math to vector2 to isolate the distance function
Math.vector2 = {};

// <Helper> Extending Math to add distance function, expects x,y properties
Math.vector2.distance = function (a, b) {
	var dx = b.x - a.x;
	var dy = b.y - a.y;

	return Math.abs(Math.sqrt(dx * dx + dy * dy));
};

// <Helper> Extending Math to add direction function
Math.vector2.direction = function (pointA, pointB) {
	var direction = {
		x: pointB.x - pointA.x,
		y: pointB.y - pointA.y,
		magnitude: 0,
		normalized: {
			x: 0,
			y: 0,
		}
	};

	direction.magnitude = Math.sqrt(direction.x * direction.x + direction.y * direction.y);
	direction.normalized.x = direction.x / direction.magnitude;
	direction.normalized.y = direction.y / direction.magnitude;

	return direction;
};

// <Helper> Font helper to maintain fonts controll in one place
Fonts = {
	TAGS: {
		MAIN: {
			NAME: 'main_font',
			PATH: 'main_font.fnt'
		},
	},
};

// <Helper> Font helper to maintain fonts controll in one place
Delays = {
	RESET: 2500,
	SHOW_OUTCOME: 1000,
	WAIT_FOR_BLINK: 2500,
};

// <Helper> Extending String for formatting
String.prototype.format = function () {
	strg = this;
	for (a in arguments) {
		strg = strg.replace("{" + a + "}", arguments[a])
	}
	return strg
}
