// setting inheritence
EndMenu.prototype = Object.create(StartMenu.prototype);
EndMenu.prototype.constructor = EndMenu;

// end game menu
function EndMenu() {
    StartMenu.call(this);
}

// initializes end menu
EndMenu.prototype.intiaize = function (screen, ticker, stage, continueCallbackCallback) {
    this.continueCallback = continueCallbackCallback;
    this.area = new PIXI.Container();
    this.background = new Background();

    //start menu buttons
    var playerAgainButton = new MenuButton('Play Again', this.onSelectCallback.bind(this));
    var menuButton = new MenuButton('Menu', this.onSelectCallback.bind(this, true));
    this.buttons = [playerAgainButton, menuButton];

    this.addToCanvas(stage, screen);
    this.background.initialize(stage, ticker, this.getAreaInfo(screen));
    this.background.getGraphics().addChild(this.area);
    this.hide(null, true);
};

// gets area info for background
EndMenu.prototype.getAreaInfo = function (screen) {
    var info = new BackgroundInfo();

    info.center.x = 0;
    info.center.y = 0;

    info.outside.x = 0;
    info.outside.y = screen.height;

    return info;
};

// adds start menu contents to area container
EndMenu.prototype.addToCanvas = function (stage, screen) {
    for (var button in this.buttons) {
        this.area.addChild(this.buttons[button].getContainer());
        this.buttons[button].setOffset(0,
            (screen.height * 0.125) + (screen.height * 0.125) * button);
    }

    this.area.position.set(screen.width * 0.5, screen.height * 0.5);
};

// end menu button selection callback
EndMenu.prototype.onSelectCallback = function (backToMenu) {
    this.hide(this.continueCallback.bind(this, backToMenu));
};
